/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/r3989/rpc_library/log"
	"gitlab.com/r3989/rpc_library/rpc"
)

// mycmdCmd represents the mycmd command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "启动rpc服务",
	Long:  `启动rpc服务`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := rpc.TcpServer.Start(); err != nil {
			log.Error("server start failed", err.Error())
		}
	},
}

func init() {
	AddCommand(serverCmd)
}
