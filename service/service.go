package service

import (
	"gitlab.com/r3989/rpc_library/cmd"
	"gitlab.com/r3989/rpc_library/log"
)

func Do() {

	if err := cmd.Execute(); err != nil {
		log.Error("start error", err.Error())
	}
}
