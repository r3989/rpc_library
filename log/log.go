package log

import (
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/r3989/rpc_library/config"
)

const logLevelName = `log_level`
const defaultLogLevel = log.InfoLevel

func init() {
	level := defaultLogLevel
	if config.IsSet(logLevelName) {
		level = log.Level(config.GetInt(logLevelName))
	}
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(level)
}

func Panic(title string, content interface{}) {
	log.WithFields(log.Fields{"title": title}).Panic(content)
}

func Fatal(title string, content interface{}) {
	log.WithFields(log.Fields{"title": title}).Fatal(content)
}

func Error(title string, content interface{}) {
	log.WithFields(log.Fields{"title": title}).Error(content)
}

func Warn(title string, content interface{}) {
	log.WithFields(log.Fields{"title": title}).Warn(content)
}

func Info(title string, content interface{}) {
	log.WithFields(log.Fields{"title": title}).Info(content)
}
func Debug(title string, content interface{}) {
	log.WithFields(log.Fields{"title": title}).Debug(content)
}
