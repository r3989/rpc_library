package zookeeper

import (
	"path"
	"strings"
	"time"

	"github.com/samuel/go-zookeeper/zk"
)

var ZkClient *zkClient

type zkClient struct {
	hosts []string
	conn  *zk.Conn
}

func init() {
	ZkClient = &zkClient{hosts: []string{"127.0.0.1:2181"}}
}

func (this *zkClient) Connect() (err error) {
	conn, _, err := zk.Connect(this.hosts, time.Second*5)
	if err != nil {
		return err
	}
	this.conn = conn
	return
}

// 创建短暂自动添加序号节点
func (this *zkClient) CreateEphemeralSequence(path string, data string) (p string, err error) {
	// flags有4种取值：
	// 0:永久，除非手动删除
	// zk.FlagEphemeral = 1:短暂，session断开则该节点也被删除
	// zk.FlagSequence  = 2:会自动在节点后面添加序号
	// 3:Ephemeral和Sequence，即，短暂且自动添加序号
	// 获取访问控制权限
	acls := zk.WorldACL(zk.PermAll)
	p, err = this.conn.Create(path, []byte(data), zk.FlagEphemeral|zk.FlagSequence, acls)

	return
}

// 创建短暂自动添加序号节点
func (this *zkClient) CreatePersist(path string, data string) (p string, err error) {
	// flags有4种取值：
	// 0:永久，除非手动删除
	// zk.FlagEphemeral = 1:短暂，session断开则该节点也被删除
	// zk.FlagSequence  = 2:会自动在节点后面添加序号
	// 3:Ephemeral和Sequence，即，短暂且自动添加序号
	// 获取访问控制权限
	acls := zk.WorldACL(zk.PermAll)
	p, err = this.conn.Create(path, []byte(data), 0, acls)

	return
}

// 创建节点（迭代）
func (this *zkClient) CreatePersistIteration(path string) error {
	if path == `/` {
		return nil
	}
	paths := strings.Split(strings.Trim(path, `/`), `/`)
	p := ""
	for _, item := range paths {
		p += "/" + item
		if isExist, err := this.Exists(p); err != nil {
			return err
		} else if !isExist {
			if _, err := this.CreatePersist(p, ``); err != nil {
				return err
			}
		} else {
		}
	}
	return nil
}

// 保存临时节点信息
func (this *zkClient) SaveEphemeralSequence(p string, data string) (string, error) {
	if err := this.CreatePersistIteration(path.Dir(p)); err != nil {
		return "", err
	} else {
		if path, err := this.CreateEphemeralSequence(p, data); err != nil {
			return "", err
		} else {
			return path, nil
		}
	}
}

// 保存持久节点信息
func (this *zkClient) SavePersist(p string, data string) error {
	if err := this.CreatePersistIteration(path.Dir(p)); err != nil {
		return err
	} else {
		if _, err := this.CreatePersist(p, data); err != nil {
			return err
		}
	}
	return nil
}

// 查询
func (this *zkClient) Get(path string) (data string, err error) {
	dataByte, _, err := this.conn.Get(path)
	data = string(dataByte)
	return
}

// 是否存在
func (this *zkClient) Exists(path string) (isExist bool, err error) {
	isExist, _, err = this.conn.Exists(path)
	return isExist, err
}

// 查询
func (this *zkClient) Set(path string, data string) error {
	_, sate, _ := this.conn.Get(path)
	_, err := this.conn.Set(path, []byte(data), sate.Version)
	return err
}

// 子路径
func (this *zkClient) Children(path string) (paths []string, err error) {
	paths, _, err = this.conn.Children(path)
	return
}

// 获取一级子节点信息
func (this *zkClient) GetFirstChildren(path string) (data map[string]string, err error) {
	paths, _, err := this.conn.Children(path)
	if err != nil {
		return nil, err
	}
	data = map[string]string{}
	for _, chldPath := range paths {
		val, err := this.Get(path + `/` + chldPath)
		if err != nil {
			return nil, err
		}
		data[chldPath] = val
	}
	return data, nil
}
