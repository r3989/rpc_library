package zookeeper

import (
	"fmt"
	"testing"
)

func TestGet(t *testing.T) {
	if err := ZkClient.Connect(); err != nil {
		t.Error(err)
	}
	data, err := ZkClient.Children("/mysql/db_auth")
	if err != nil {
		t.Error(err)
	}
	fmt.Println("data:", data)
}

func TestGetFirstChildren(t *testing.T) {
	if err := ZkClient.Connect(); err != nil {
		t.Error(err)
	}
	data, err := ZkClient.GetFirstChildren("/mysql/db_auth")
	if err != nil {
		t.Error(err)
	}
	fmt.Println("data:", data)
}

func TestSaveEphemeralSequence(t *testing.T) {
	if err := ZkClient.Connect(); err != nil {
		t.Error(err)
	}
	err := ZkClient.SaveEphemeralSequence("/web/www/url", "www.soyoung.com")
	if err != nil {
		t.Error(err)
	}
}

func TestCreateEphemeralSequenceIteration(t *testing.T) {
	if err := ZkClient.Connect(); err != nil {
		t.Error(err)
	}
	err := ZkClient.CreatePersistIteration("/web/www/url")
	if err != nil {
		t.Error(err)
	}
}
