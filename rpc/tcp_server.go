package rpc

import (
	"fmt"

	"github.com/hprose/hprose-golang/rpc"
	"gitlab.com/r3989/rpc_library/config"
	"gitlab.com/r3989/rpc_library/log"
)

const defaultPort = 8080
const portKey = `port`

var TcpServer tcpServer

type RpcOptions rpc.Options

func init() {
	port := defaultPort
	if config.IsSet(portKey) {
		port = config.GetInt(portKey)
	}
	url := fmt.Sprintf("tcp4://127.0.0.1:%d", port)
	TcpServer = tcpServer{rpc.NewTCPServer(url)}
}

type tcpServer struct {
	*rpc.TCPServer
}

func (t *tcpServer) Start() error {
	log.Info("rpc", "启动服务")
	return t.TCPServer.Start()
}

func AddFunction(name string, function interface{}, option ...RpcOptions) {
	TcpServer.AddFunction(name, function, toHproseRpcOptions(option)...)
}

func AddFunctions(names []string, functions []interface{}, option ...RpcOptions) {
	TcpServer.AddFunctions(names, functions, toHproseRpcOptions(option)...)
}

func AddMethod(name string, obj interface{}, alias string, option ...RpcOptions) {
	TcpServer.AddMethod(name, obj, alias, toHproseRpcOptions(option)...)
}

func AddAllMethods(obj interface{}, option ...RpcOptions) {
	TcpServer.AddAllMethods(obj, toHproseRpcOptions(option)...)
}

func AddInstanceMethods(obj interface{}, option ...RpcOptions) {
	TcpServer.AddInstanceMethods(obj, toHproseRpcOptions(option)...)
}

func toHproseRpcOptions(options []RpcOptions) []rpc.Options {
	if options == nil || len(options) == 0 {
		return make([]rpc.Options, 0)
	}
	ret := make([]rpc.Options, len(options))
	for i, o := range options {
		ret[i] = rpc.Options(o)
	}
	return ret
}
