package config

import (
	"github.com/spf13/viper"
)

func init() {
	parseConfig()
}

func parseConfig() {
	confFile, err := getConfPath("")
	if err != nil {
		panic(err)
	}
	viper.SetConfigFile(confFile)
	err = viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func GetString(key string) string {
	return viper.GetString(key)
}

func GetBool(key string) bool {
	return viper.GetBool(key)
}

func GetInt(key string) int {
	return viper.GetInt(key)
}

func IsSet(key string) bool {
	return viper.IsSet(key)
}

func GetAppName() string {
	return GetString("app_name")
}
