package config

import (
	"fmt"
	"os"
)

// 获取配置文件路径
func getConfPath(confFile string) (string, error) {
	if len(confFile) > 0 {
		return confFile, nil
	}
	defaultDir, err := os.Getwd()
	if err != nil {
		return ``, err
	}
	file := fmt.Sprintf("%s/conf/config.yaml", defaultDir)
	return file, nil
}
